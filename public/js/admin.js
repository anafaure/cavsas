$(() => {
    $('.container .row form.form-add-company div.errDescripcion').hide()
    $('.container .row form.form-add-company div.errCuit').hide()

    renderCombo($('.form-add-user #company'), baseURL + '/api/company', 'Seleccione empresa...')
    hiddenForm() //errores
    $('.container .row form.form-add-user div.company').hide()
})
/////////////////////////////////////////////////////////////////////
//////////  COMPANIES  
/////////////////////////////////////////////////////////////////////
$('.container .row form.form-add-company button.btn-submit').on('click', e => {
    let flag = true
    $('.container .row form.form-add-company div.errDescripcion').hide()
    $('.container .row form.form-add-company div.errCuit').hide()
    let descripcion = $('.container .row form.form-add-company #descripcion').val().trim()
    let cuit = Number($('.container .row form.form-add-company #cuit').val().trim())
    if (descripcion === '') {
        $('.container .row form.form-add-company div.errDescripcion').show()
        flag = false
    }
    if (cuit === '') {
        $('.container .row form.form-add-company div.errCuit').show()
        flag = false
    }
    if (flag) {
        let razonSocial = $('.container .row form.form-add-company #razonSocial').val().trim()
        // console.log(countCompanies, descripcion, cuit, razonSocial)
        fetch(baseURL + '/api/company', {
            method: 'put',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ id: countCompanies + 1, descripcion, cuit, razonSocial })
        }).then(resp => { return resp.json() }).then(data => {
            if (data.error) { alert(data.message) }
            window.location.replace(baseURL + '/dashboard/companies');
        })


    }

})
// $('.container .row form.form-add-company #cuit').blur(()=>{
//     // alert('ajhdbv')
// })
$('.container .row form.form-add-company #cuit').focusout(() => {
    let cuit = Number($('.container .row form.form-add-company #cuit').val().trim())
    if (cuit === 0) {
        $('.container .row form.form-add-company div.errCuit').show()
        $('.container .row form.form-add-company #cuit').focus()
    }


})
/////////////////////////////////////////////////////////////////////
//////////  USERS
/////////////////////////////////////////////////////////////////////
$('.container .row form.form-add-user button.btn-submit').on('click', e => {
    hiddenForm()
    let complete = true,
        role = $('.container .row form.form-add-user #role').val(),
        cuil = $('.container .row form.form-add-user #cuil').val(),
        lastname = $('.container .row form.form-add-user #lastname').val(),
        name = $('.container .row form.form-add-user #name').val(),
        phone = $('.container .row form.form-add-user #phone').val(),
        email = $('.container .row form.form-add-user #email').val(),
        username = $('.container .row form.form-add-user #username').val(),
        password = $('.container .row form.form-add-user #password').val(),
        company = $('.container .row form.form-add-user #company').val(),
        descripcion = $('.container .row form.form-add-company #descripcion').val()

    if (role === 'SEGURO' && company === '') { complete = false; $('.container .row form.form-add-user div.errCompany').show() }
    if (role === '') { complete = false; $('.container .row form.form-add-user div.errRole').show() }
    if (cuil === '') { complete = false; $('.container .row form.form-add-user div.errCuil').show() }
    if (lastname === '') { complete = false; $('.container .row form.form-add-user div.errLastname').show() }
    if (name === '') { complete = false; $('.container .row form.form-add-user div.errName').show() }
    if (email === '') { complete = false; $('.container .row form.form-add-user div.errEmail').show() }
    if (phone === '') { complete = false; $('.container .row form.form-add-user div.errPhone').show() }
    if (username === '') { complete = false; $('.container .row form.form-add-user div.errUsername').show() }
    if (password === '') { complete = false; $('.container .row form.form-add-user div.errPassword').show() }
    if (descripcion === '') { complete = false; $('.container .row form.form-add-company div.errDescripcion').show() }

    if (complete) {
        fetch(baseURL + '/api/user', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ username, role, cuil, lastname, name, phone, email, password, company })
        }).then(resp => { return resp.json() }).then(data => {
            // console.log(data)
            if (data.error) { console.log(data); alert(data.message) }
            window.location.replace(baseURL + '/dashboard/users');
        })
    }
})
hiddenForm = () => {
    $('.container .row form.form-add-user div.errRole').hide()
    $('.container .row form.form-add-user div.errCompany').hide()
    $('.container .row form.form-add-user div.errCuil').hide()
    $('.container .row form.form-add-user div.errLastname').hide()
    $('.container .row form.form-add-user div.errName').hide()
    $('.container .row form.form-add-user div.errEmail').hide()
    $('.container .row form.form-add-user div.errPhone').hide()
    $('.container .row form.form-add-user div.errUsername').hide()
    $('.container .row form.form-add-user div.errPassword').hide()

}
$('.form-add-user #role').on('change', () => {
    if ($('.form-add-user #role').val() === 'SEGURO') {
        $('.container .row form.form-add-user div.company').show()
    } else {
        $('.container .row form.form-add-user div.company').hide()
    }
})
/////////////////////////////////////////////////////////////////////
//////////  SINESTERS 
/////////////////////////////////////////////////////////////////////
loadModal = id => {
    $('#sinisterModal').modal({ backdrop: 'static', keyboard: false })
    fetch(baseURL + '/api/sinister/' + id).then(resp => { return resp.json() }).then(async data => {
        console.log(data)
        $('#sinisterModal #codeSinister').val(id)
        await renderCombo($('#sinisterModal #perito'), baseURL + '/api/userBy/PERITO', 'Seleccione perito')
        await renderCombo($('#sinisterModal #tallerista'), baseURL + '/api/userBy/TALLERISTA', 'Seleccione tallerista')
        if (data.perito !== null) $('#sinisterModal #perito').val(data.perito.cuil)
        if (data.tallerista !== null) $('#sinisterModal #tallerista').val(data.tallerista.cuil)

    })
}
$('#sinisterModal button.btn-assign').on('click', (e) => {
    let cuilPerito = $('#sinisterModal #perito').val(),
        cuilTallerista = $('#sinisterModal #tallerista').val()
    fetch(baseURL + '/api/sinister/updateAssign', {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            idSinister: $('#sinisterModal #codeSinister').val(),
            cuilPerito: (cuilPerito !== '') ? cuilPerito : null,
            cuilTallerista: (cuilTallerista !== '') ? cuilTallerista : null
        })
    }).then(resp => { return resp.json() }).then(data => {
        console.log(data)
        location.reload()
    }).catch(err => { console.log(err) })
})