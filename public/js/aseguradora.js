$(() => {
    $('.data-client').hide()
    $('#polizaNotFound').hide()
    $('#errores').hide()
})
$('.form-poliza #poliza').on('keypress', (e) => {
    if (e.which === 13) {
        let poliza = $('.form-poliza #poliza').val().trim()
        if (poliza !== '') clientFormulary(poliza)
        else {
            alert('Debe completar el número de póliza.')
        }
    }
})
// $('.form-poliza #poliza').on('blur', () => {
//     let poliza = $('.form-poliza #poliza').val().trim();
//     if (poliza === '') alert('Debe completar el número de póliza.')
//     $('.form-poliza #poliza').focus()
// })
$('form.form-insurance button.btn-submit').on('click', (e) => {
    let errs = [], client = {}, poliza = $('.form-poliza #poliza').val().trim()
    client.cuil = $('form.form-insurance #cuil').val()
    client.lastname = $('form.form-insurance #lastname').val()
    client.name = $('form.form-insurance #name').val()
    client.email = $('form.form-insurance #email').val()
    client.phone = $('form.form-insurance #phone').val()
    client.nroSinister = $('form.form-insurance #nroSinister').val()

    errs = evalEmpty(client)
    if (errs.length > 0) { $('#errores').show(); e.preventDefault() }
    else {
        $('form.form-insurance input[name=company]').val(company)
        $('form.form-insurance input[name=poliza]').val(poliza)
        $('form.form-insurance').submit()
    }
})

clientFormulary = poliza => {
    $('.form-poliza #poliza').prop('disabled', true)
    fetch(baseURL + '/api/insurance/' + poliza+'/'+company).then(resp => { return resp.json() }).then(data => {
        if (data.error) { alert(data.message) }
        else {
            $('.data-client').show(500)
            if (!data.message) { // si encuentra
                $('#polizaNotFound').hide()
                $('form.form-insurance #cuil').val(data.client.cuil)
                $('form.form-insurance #name').val(data.client.name)
                $('form.form-insurance #lastname').val(data.client.lastname)
                $('form.form-insurance #email').val(data.client.email)
                $('form.form-insurance #phone').val(data.client.phone)
                $('form.form-insurance #nroSinister').val('')
                disabledInputs(true)
            } else { // crear nuevo
                $('form.form-insurance #cuil').val('')
                $('form.form-insurance #name').val('')
                $('form.form-insurance #lastname').val('')
                $('form.form-insurance #email').val('')
                $('form.form-insurance #phone').val('')
                $('form.form-insurance #nroSinister').val('')
                disabledInputs(false)
                $('#polizaNotFound').show()
                $('form.form-insurance #cuil').focus()
            }

        }
    }).catch(e=>{console.log(e)})
}
disabledInputs = flag => {
    $('form.form-insurance #cuil').prop('disabled', flag)
    $('form.form-insurance #name').prop('disabled', flag)
    $('form.form-insurance #lastname').prop('disabled', flag)
}






////////////////////// para los combos dependientes
// $(() => {
    //     renderCombo(cmbProv, baseURL + '/api/province', '<option class="desabled" value="">Seleccione provincia</option>')
    // })
    // let cmbProv = $('form #cmb_prov'), cmbDpto = $('form #cmb_dpto'), cmbLoc = $('form #cmb_city')

    // combos events
    // cmbProv.on('change', () => {
        //     cmbDpto.html('<option class="desabled" value="">Debe seleccionar provincia</option>')
        //     cmbLoc.html('<option class="desabled" value="">Debe seleccionar departamento</option>')
        //     renderCombo(cmbDpto, baseURL + '/api/departament/province/' + cmbProv.val(), '<option class="desabled" value="">Seleccione departamento</option>')
        // })
    // cmbDpto.on('change', () => {
        //     cmbLoc.html('<option class="desabled" value="">Debe seleccionar departamento</option>')
        //     renderCombo(cmbLoc, baseURL + '/api/city/departament/' + cmbDpto.val(), '<option class="desabled" value="">Seleccione localidad</option>')
        // })