
$(() => {
    $('form.form-code .codeEmpty').hide()

    $('.generate-code .generator-code').hide()
    $('.generator-code .emptyField').hide()

    $('.generate-code #ckb-code').prop('checked', false)
    renderCombo($('.form-generate #company'), baseURL+'/api/company', 'Seleccione su aseguradora.')
})

$('.generate-code #ckb-code').on('click', () => { $('.container .generate-code .generator-code').toggle(400) })

$('.form-generate button').on('click', (e) => {
    $('.generator-code .emptyField').hide()
    e.preventDefault()
    let data = {}
    data.poliza = $('.form-generate input#poliza').val()
    data.cuil = $('.form-generate input#cuil').val()
    data.email = $('.form-generate input#email').val()
    data.company = $('.form-generate #company').val()

    let err = evalEmpty(data)
    if (err.length > 0) $('.generator-code .emptyField').show()
    else {      
        fetch(baseURL + '/api/generator-code', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ data })
        }).then(resp => { return resp.json() }).then(data => {
            if (data.error) { console.log(data); alert(data.message) }
            let msg = (data.state) ? ('Código se seguimiento: ' + data.code) : data.message
            $('.generator-code .code-generated p').html(msg)
        })
    }
})

$('form.form-code button').on('click', (e) => {
    if ($('form.form-code input#code').val().trim() === '') {
        e.preventDefault()
        $('form.form-code .codeEmpty').show()
    } else $('form.form-code').submit()
})