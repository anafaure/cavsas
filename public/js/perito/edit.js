let prov = $('.lugar-siniestro #province'), dpto = $('.lugar-siniestro #departament'), city = $('.lugar-siniestro #city')
// let licences = []
$(() => {
    renderCombo(prov, `${baseURL}/api/province`, 'Seleccione una provincia.')
    dpto.html('<option value="" class="disabled">Debe seleccionar provincia.</option>')
    city.html('<option value="" class="disabled">Debe seleccionar un departamento.</option>')
    getAndRenderLicences()
})
getAndRenderLicences = async () => {
    fetch(`${baseURL}/api/client/${cuilClient}`).then(resp => { return resp.data }).then(data => {  console.log(data) })

}
$('.info-asegurado .modal-updateClient').on('click', () => { // open modal

})
$('#modalClient .btn-save-client').on('click', (e) => { // update  data client
    let client = {}
    client.oid = $('.form-client input#codeClient').val()
    client.gender = ($('.form-client #sexF').is(":checked")) ? $('.form-client #sexF').val() : $('.form-client #sexM').val()
    client.province = prov.val()
    client.departament = dpto.val()
    client.localidad = city.val()
    client.address = $('.form-client #address').val()
    client.profession = $('.form-client #profession').val()
    client.status = $('.form-client #status').val()
    if (evalEmpty(client).length !== 0) {
        alert('Deben completar todos los campos.')
        console.log(client, evalEmpty(client))
        e.preventDefault()
        e.stopPropagation()
    } else {
        fetch(baseURL + '/api/client', {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(client)
        }).then(resp => { return resp.json() }).then(data => {
            if (data.success) {
                alert(data.message)
            } else {
                console.log(data)
                alert(data.message)
                e.preventDefault()
                e.stopPropagation()
            }
        }).catch(err => {
            alert('Ha habido un error al actualizar los datos.')
            console.log(err)
            e.preventDefault()
            e.stopPropagation()
        })
    }
})
///////////////////////////////////////////////////
$('#modalLicence .modal-footer button.btn-save-licence').on('click', (e) => {
    let licence = {}
    licence.cuil = $('#modalLicence .form-add-licence #cuil').val()
    licence.clase = $('#modalLicence .form-add-licence #clase').val()
    licence.nro = $('#modalLicence .form-add-licence #nro').val()
    licence.fechaExpedicion = $('#modalLicence .form-add-licence #fechaExpedicion').val()
    licence.fechaVencimiento = $('#modalLicence .form-add-licence #fechaVencimiento').val()
    licence.otorgado = $('#modalLicence .form-add-licence #otorgado').val()
    if (evalEmpty(licence).length !== 0) {
        alert('Deben completar todos los campos.')
        e.preventDefault()
        e.stopPropagation()
    } else {
        fetch(baseURL + '/api/licence', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ cuil: licence.cuil, clase: licence.clase, nro: licence.nro, fechaExpedicion: licence.fechaExpedicion, fechaVencimiento: licence.fechaVencimiento, otorgado: licence.otorgado })
        }).then(resp => { return resp.json() }).then(data => {
            console.log(data)
            if (data.success) {
                alert(data.message)
                location.reload(true)
            } else {
                console.log(data)
                alert(data.message)
                e.preventDefault()
                e.stopPropagation()
            }
        }).catch(err => {
            alert('Ha habido un error al actualizar los datos.')
            console.log(err)
            e.preventDefault()
            e.stopPropagation()
        })
    }
})
///////////////////////////////////////////////////
prov.on('change', () => {
    renderCombo(dpto, `${baseURL}/api/departament/province/${prov.val()}`, 'Seleccione un departamento.')
    city.html('<option value="" class="disabled">Debe seleccionar un departamento.</option>')
})
dpto.on('change', () => {
    renderCombo(city, `${baseURL}/api/city/departament/${dpto.val()}`, 'Seleccione una localidad.')
})
