let prov = $('.form-client #province'), dpto = $('.form-client #departament'), city = $('.form-client #city')
$(() => {
    // renderCombo(prov, `${baseURL}/api/province`, 'Seleccione una provincia.')
    // dpto.html('<option value="" class="disabled">Debe seleccionar provincia.</option>')
    // city.html('<option value="" class="disabled">Debe seleccionar un departamento.</option>')

})
$('.client-data .btn-info-client').on('click', (e) => {

    // fetch(`${baseURL}/api/sinister/${oidSinister}`).then(resp => { return resp.json() }).then(data => {
    //     console.log(data)
    // }).catch(err => { console.log(err) })
})
$('#modalClient .form-client button.btn-submit-client').on('click', (e) => {
  e.preventDefault()
  e.stopPropagation()
    let data = {}
    data.gender = ($('.form-client #sexF').is(":checked")) ? $('.form-client #sexF').val() : $('.form-client #sexM').val()
    data.province = prov.val()
    data.departament = dpto.val()
    data.localidad = city.val()
    data.address = $('.form-client #address').val()
    data.profession = $('.form-client #profession').val()
    data.status = $('.form-client #status').val()
    if (evalEmpty(data).length !== 0) {
        alert('Deben completar todos los campos.')
        console.log(data, evalEmpty)
        e.preventDefault()
        e.stopPropagation()
    } else {
        $('.form-client').submit()
    }
})
// renderCheckRow = (list, id) => {
//     let retorno = `<h5>${id}</h5>`
//     retorno += '<div class="row">'
//     list.forEach(i => {
//         retorno += '<div class="col-md-3">'
//         retorno += '<div class="custom-control custom-checkbox">'
//         retorno += `<input type="checkbox" class="custom-control-input" name="${minusculas(i)}${id}" id="${minusculas(i)}${id}">`
//         retorno += `<label class="custom-control-label" for="${minusculas(i)}${id}">${capitalize(i)}</label>`
//         retorno += '</div></div>'
//     })
//     retorno += '</div>'
//     todo += retorno
//     return retorno
// }

//////////////////////////
prov.on('change', () => {
    renderCombo(dpto, `${baseURL}/api/departament/province/${prov.val()}`, 'Seleccione un departamento.')
    city.html('<option value="" class="disabled">Debe seleccionar un departamento.</option>')
})
dpto.on('change', () => {
    renderCombo(city, `${baseURL}/api/city/departament/${dpto.val()}`, 'Seleccione una localidad.')
})