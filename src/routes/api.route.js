const routes = require('express').Router(),
    ctrlUser = require('../controllers/api/api.controller.user'),
    ctrlUbication = require('../controllers/api/api.controller.ubication'),
    ctrlInsurance = require('../controllers/api/api.controller.insurance'),
    crtlClient = require('../controllers/api/api.controller.client')

// users
routes.get('/user', ctrlUser.getAllUsers)
routes.get('/user/:cuil', ctrlUser.getUser)
routes.get('/userBy/:role', ctrlUser.getUsersByRole)
routes.post('/user', ctrlUser.addUser)

routes.post('/client', crtlClient.addClient) // cliente nuevo a nuestro sistema
routes.put('/client/contact', crtlClient.updateClientContact) // update data cpnotact client
routes.put('/client/:cuil', crtlClient.updateClient) // listado todos
routes.get('/client', crtlClient.getAllClient) // listado todos
routes.get('/client/:cuil', crtlClient.getClient) // get info cuil

// company
routes.get('/company', ctrlUser.getCompany)
routes.put('/company', ctrlUser.addCompany) 

// generator code
routes.post('/generator-code', ctrlInsurance.generateCode)

// company
routes.get('/insurance/:poliza/:company', ctrlInsurance.getInsurance)
routes.post('/insurance', ctrlInsurance.addInsurance)
routes.post('/sinister', ctrlInsurance.addSinister)
routes.post('/sinisterState', ctrlInsurance.addSinisterState)
//routes.post('/update-client',ctrlInsurance.addInsurance)

///////////
routes.put('/sinister/updateAssign', ctrlInsurance.assignProfessionals)
routes.get('/sinister', ctrlInsurance.getAllSinister)
routes.get('/sinisterBy/:cuil', ctrlInsurance.getSinisterAssing)
routes.get('/sinister/:code', ctrlInsurance.getSinister)
routes.get('/sinisterClient/:code', ctrlInsurance.getSinisterClient)
routes.put('/updateSinester/id', ctrlInsurance.updatePerito)

routes.post('/licence', crtlClient.addLicence)
routes.post('/vehicule', crtlClient.addVehicule)
// routes.get('/licence/:cuil', crtlClient.getLicence)
// routes.get('/vehicule/:cuil', crtlClient.getVehicule)



// ubication
routes.get('/province', ctrlUbication.getAllProvince)
routes.post('/province', ctrlUbication.addProvince)
routes.get('/departament', ctrlUbication.getAllDepto)
routes.post('/departament', ctrlUbication.addDepartament)
routes.get('/departament/province/:id', ctrlUbication.getDptoByProvince)
routes.get('/city', ctrlUbication.getAllCity)
routes.post('/city', ctrlUbication.addCity)
routes.get('/city/:id', ctrlUbication.getCity)
routes.get('/city/departament/:id', ctrlUbication.getCityByDpto)
// routes.get('/etl', ctrlUbication.etl)
routes.get('/etl', ctrlUbication.etl)
module.exports = routes