const routes = require('express').Router(),
    config = require('../config/init'),
    // Cryptr = require('cryptr'),
    // cryptr = new Cryptr(config.encrypt),
    { isAuthenticated } = require('../middlewares/middleware'),
    ctrlLogin = require('../controllers/controller.login'),
    ctrlAdmin = require('../controllers/controller.admin'),
    ctrlCommon = require('../controllers/controller.common')

// index
routes.get('/', ctrlCommon.viewIndex)
routes.get('/tracing', ctrlCommon.viewCode)
routes.post('/tracing', ctrlCommon.searchTracing) // redirect summary

// login
routes.post('/login', ctrlLogin.validate)
routes.get('/logout', ctrlLogin.logout)
routes.get('/login', ctrlLogin.viewLogin)
/////////////////////////////////////////////////////////////////////////

//users logged
routes.get('/home', isAuthenticated, ctrlAdmin.viewHome)

// admin
routes.get('/dashboard', isAuthenticated, ctrlAdmin.viewDashboard)
routes.get('/dashboard/users', isAuthenticated, ctrlAdmin.viewUsers)
routes.get('/dashboard/companies', isAuthenticated, ctrlAdmin.viewCompany)

routes.get('/proficient', isAuthenticated, ctrlAdmin.viewPerito)
routes.get('/proficient/:oid', isAuthenticated, ctrlAdmin.viewPeritoEdit)
routes.post('/proficient', isAuthenticated,ctrlAdmin.loadSiniter)
routes.get('/garage', isAuthenticated, ctrlAdmin.viewTallerista)
routes.post('/garage', isAuthenticated,ctrlAdmin.loadSiniter)
routes.get('/company', isAuthenticated, ctrlAdmin.viewCompania)

routes.post('/updateSinester', isAuthenticated, ctrlCommon.viewloadPerito)
// add/update client insuranced
routes.post('/company/client', isAuthenticated, ctrlCommon.addClient)


// encrypt
// routes.get('/enc', (req, res) => {
//     let pass = '123452', // cuil or sinister id?
//         encryptedString = cryptr.encrypt(pass)
//     res.send(encryptedString)
// })
// routes.get('/des', (req, res) => {
//     let pass = '9986dd9de7305794546c2866a2b49ab35d54863358a56ece0944b8'
//     let decryptedString = cryptr.decrypt(pass)
//     res.send(decryptedString)
// })
module.exports = routes