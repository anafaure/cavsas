const express = require('express'),
    path = require('path'),
    flash = require('connect-flash-plus'),
    fileUpload = require('express-fileupload'),
    logger = require('morgan'),
    favicon = require('serve-favicon'),
    cors = require('cors'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    config = require('./config/init'),
    app = express()

// config
app.set(require('./config/database'))
app.set('port', process.env.PORT || config.port)
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

// middlewares
app.use(favicon(path.join(__dirname, '/../public', 'img', 'favicon.ico')))
app.use('/static', express.static(__dirname + './../public'))
app.use('/bootstrap', express.static(__dirname + './../node_modules/bootstrap/dist/css'))
app.use('/bootstrap/js', express.static(__dirname + './../node_modules/bootstrap/dist/js'))
app.use('/jquery', express.static(__dirname + './../node_modules/jquery/dist'))
app.use(logger('dev'))
app.options('*', cors())
app.use(fileUpload())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(session({
    secret: config.session.secretPass,
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: config.session.maxAge }
}))
app.use(flash())

// routers
app.use('/', require('./routes/main.route'))
app.use('/api', require('./routes/api.route'))

app.listen(app.get('port'), () => { console.log('Listening to port: ' + app.get('port')) })