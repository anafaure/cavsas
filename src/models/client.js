const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    uniqueValidator = require('mongoose-unique-validator'),

    ClientSchema = new Schema({
        // campos obligatorios
        cuil: { type: Number, unique: true, required: [true, "No puede ser nulo"] },
        lastname: { type: String, required: [true, "No puede ser nulo"] },
        name: { type: String, required: [true, "No puede ser nulo"] },
        phone: { type: String, match: [/^(([+]|[0]{2})([\d]{1,3}))?([\d]{10})$/, 'No es valido'], required: [true, "No puede ser nulo"], },
        email: { type: String, lowercase: true, required: [true, "No puede ser nulo"], match: [/\S+@\S+\.\S+/, 'No es valido'] },
        // campos nuleables, que no completa la aseguradora
        licence: { type: Schema.Types.ObjectId, ref: 'Licence' },
        vehicule: [{ type: Schema.Types.ObjectId, ref: 'Vehicule', "default": [] }],
        address: { type: String },
        city: { type: mongoose.Schema.Types.ObjectId, ref: 'City' },
        profession: { type: String },
        gender: { type: String, enum: ['FEMENINO', 'MASCULINO'] },
        status: {
            type: String,
            enum: ['SOLTERO', 'COMPROMETIDO', 'CASADO', 'SEPARADO', 'DIVORCIADO', 'VIUDO']
        }
    }, { timestamps: true });

ClientSchema.plugin(uniqueValidator, { message: 'Ya existe el usuario' });

module.exports = mongoose.model('Client', ClientSchema)
