const mongoose = require('mongoose'),
    Schema = mongoose.Schema,

    InsuranceSchema = Schema({
        company: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: true },
        poliza: { type: Number, unique: true, required: [true, "No puede ser nulo"] },
        client: { type: mongoose.Schema.Types.ObjectId, ref: 'Client', required: [true, "No puede ser nulo"] }
    }, { timestamps: true })

module.exports = mongoose.model('Insurance', InsuranceSchema)
