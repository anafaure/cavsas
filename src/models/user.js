const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    uniqueValidator = require('mongoose-unique-validator'),
    crypto = require('crypto'),
    // mongooseHidden = require('mongoose-hidden')(),

    UserSchema = Schema({
        username: { type: String, unique: true, require: [true, "No puede ser nulo"] },
        cuil: { type: Number, unique: true, required: [true, "No puede ser nulo"] },
        role: { type: String, enum: ['ADMIN', 'SEGURO', 'PERITO', 'TALLERISTA'], required: [true, "No puede ser nulo"] },
        lastname: { type: String, required: [true, "No puede ser nulo"] },
        company: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: false },
        name: { type: String, required: [true, "No puede ser nulo"] },
        phone: { type: String, match: [/^(([+]|[0]{2})([\d]{1,3}))?([\d]{10})$/, 'No es valido'] },
        email: { type: String, lowercase: true, required: [true, "No puede ser nulo"], match: [/\S+@\S+\.\S+/, 'No es valido'] },
        hash: { type: String, hide: true },
        salt: { type: String, hide: true },
    }, { timestamps: true })

UserSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
}

UserSchema.methods.validPassword = function (password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
}
UserSchema.methods.toJSON = function () {
    return {
        username: this.username,
        cuil: this.cuil,
        role: this.role,
        lastname: this.lastname,
        name: this.name,
        phone: this.phone,
        email: this.email,
        company: this.company
    }
}
UserSchema.plugin(uniqueValidator, { message: 'Ya existe el usuario' });
// UserSchema.plugin(mongooseHidden)

module.exports = mongoose.model('User', UserSchema)
