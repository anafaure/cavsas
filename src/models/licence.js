const mongoose = require('mongoose'),
    Schema = mongoose.Schema,


    LicenceSchema = new Schema({
        clase: { type: String, required: [true, "No puede ser nulo"] },
        nro: { type: Number, required: [true, "No puede ser nulo"] },
        fechaExpedicion: { type: Date, required: [true, "No puede ser nulo"] },
        fechaVencimiento: { type: Date, required: [true, "No puede ser nulo"] },
        otorgado: { type: String, required: [true, "No puede ser nulo"] }//quien emite String
    })
    module.exports = mongoose.model('Licence', LicenceSchema)
