const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    SinisterStateSchema = Schema({
        descripcion: { type: String, unique: true  }
        
    }, { timestamps: true })

module.exports = mongoose.model('SinisterState', SinisterStateSchema)