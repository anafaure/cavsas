const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    mongooseHidden = require('mongoose-hidden')(),

    ProvinceSchema = Schema({
        id: { type: Number, unique: true },
        descripcion: { type: String }
    })
    ProvinceSchema.plugin(mongooseHidden)

module.exports = mongoose.model('Province', ProvinceSchema)
