const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    // mongooseHidden = require('mongoose-hidden')(),


    DepartamentSchema = Schema({
        id: { type: Number, unique: true },
        descripcion: { type: String, required: true },
        province: { type: mongoose.Schema.Types.ObjectId, ref: 'Province' }
    })
// DepartamentSchema.plugin(mongooseHidden)
module.exports = mongoose.model('Departament', DepartamentSchema)
