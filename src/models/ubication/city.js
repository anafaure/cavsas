const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    mongooseHidden = require('mongoose-hidden')(),

    CitySchema = Schema({
        id: { type: Number, unique: true },
        descripcion: { type: String },
        departament: { type: mongoose.Schema.Types.ObjectId, ref: 'Departament' }
    })
    CitySchema.plugin(mongooseHidden)

module.exports = mongoose.model('City', CitySchema)
