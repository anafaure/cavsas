const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    mongooseHidden = require('mongoose-hidden')(),

    CompanySchema = new Schema({
        id: { type: Number, required: true },
        descripcion: { type: String, required: true },
        cuit: { type: Number, required: true },
        razonSocial: { type: String }
    }, { timestamps: true })

// CompanySchema.methods.toJSON = function () {
//     return {
//         id: this.id,
//         descripcion: this.descripcion
//     }
// }
CompanySchema.plugin(mongooseHidden)
module.exports = mongoose.model('Company', CompanySchema)
