const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    SinesterSchema = Schema({
        client: { type: mongoose.Schema.Types.ObjectId, ref: 'Client', required: [true, "No puede ser nulo"] },
        perito: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default: null },
        tallerista: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default: null },
        sinesterState: { type: mongoose.Schema.Types.ObjectId, ref: 'SinesterState', default: null },
        nroSinister: { type: Number, required: true },
        finished: { type: Boolean, default: false },
        // details sinister
        //citySinister: { type: mongoose.Schema.Types.ObjectId, ref: 'City' },
        streat: { type: String },
        numberKm: { type: Number }, // o km
        cp: { type: Number },
        dateSinister: { type: Date },
        // Detalles de la partes afectadas del vehiculo
        guardabarro: [{ type: String, enum: ['DELANTERO DERECHA', 'DELANTERO IZQUIERDA', 'TRACERO DERECHO', 'TRACERO IZQUIERDO'] }],
        puerta: [{ type: String, enum: ['DELANTERO DERECHA', 'DELANTERO IZQUIERDA', 'TRACERO DERECHO', 'TRACERO IZQUIERDO'] }],
        faro: [{ type: String, enum: ['DELANTERO DERECHA', 'DELANTERO IZQUIERDA', 'TRACERO DERECHO', 'TRACERO IZQUIERDO'] }],
        neumatico: [{ type: String, enum: ['DELANTERO DERECHA', 'DELANTERO IZQUIERDA', 'TRACERO DERECHO', 'TRACERO IZQUIERDO'] }],
        llanta: [{ type: String, enum: ['DELANTERO DERECHA', 'DELANTERO IZQUIERDA', 'TRACERO DERECHO', 'TRACERO IZQUIERDO'] }],
        butaca: [{ type: String, enum: ['DELANTERO DERECHA', 'DELANTERO IZQUIERDA', 'TRACERO DERECHO', 'TRACERO IZQUIERDO'] }],
        cinturon: [{ type: String, enum: ['DELANTERO DERECHA', 'DELANTERO IZQUIERDA', 'TRACERO DERECHO', 'TRACERO IZQUIERDO'] }],
        retrovisor: [{ type: String, enum: ['DERECHA', 'IZQUIERDA'] }],
        parante: [{ type: String, enum: ['DERECHA', 'IZQUIERDA'] }],
        paragolpe: [{ type: String, enum: ['DELANTERO', 'TRACERO'] }],
        limpiaparabrisa: [{ type: String, enum: ['DELANTERO', 'TRACERO'] }],
        otros: [{
            type: String,
            enum: ['CAPOT', 'TECHO', 'INTERIOR', 'INSTRUMENTOS', 'MOTOR', 'TRANSMISION', 'SUSPENSION', 'DIRECCION', 'PARRILLA', 'VOLANTE', 'AIRBAG', 'PARABRISAS', 'LUNETAS', 'TAPIZADOS', 'TECHO INTERNO', 'VIDRIOS TECHO SOLAR']
        }]
    }, { timestamps: true })

module.exports = mongoose.model('Sinester', SinesterSchema)
