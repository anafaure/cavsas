const mongoose = require('mongoose'),
    Schema = mongoose.Schema,

VehiculeSchema = new Schema({
    marca: { type: String, required: [true, "No puede ser nulo"] },
    modelo: { type: String, required: [true, "No puede ser nulo"] },
    dominio: { type: String, required: [true, "No puede ser nulo"] },
    año: { type: Number },
    color: { type: String, required: [true, "No puede ser nulo"] },
    nroChasis: { type: String, required: [true, "No puede ser nulo"] },
    nroMotor: { type: String, required: [true, "No puede ser nulo"] },
    tipo: { type: String, required: [true, "No puede ser nulo"] }, // descripcion libre String 
    inspeccionTecnica: { type: Boolean, required: [true, "No puede ser nulo"], default: false },
    tipoCargaTransportada: { type: String, required: [true, "No puede ser nulo"] }// descripcion libre String 
})
module.exports = mongoose.model('Vehicule', VehiculeSchema)
