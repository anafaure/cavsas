let axios = require('axios'),
    multimedia = require('../helpers/multimedia')

requestAxios = (request, url, obj) => {
    if (request === 'GET') return axios.get(url).then(data => { return data.data })
    if (request === 'POST') return axios.post(url, obj).then(data => { return data.data })
    if (request === 'PUT') return axios.put(url, obj).then(data => { return data.data })

}

exports.viewIndex = (req, res) => {
    res.render('index', {
        urlBase: `http://${req.get('host')}`,
        user: (req.session.userLogged) ? req.session.userLogged : undefined
    })
}
exports.viewCode = (req, res) => {
    res.render('tracing', {
        urlBase: `http://${req.get('host')}`,
        user: (req.session.userLogged) ? req.session.userLogged : undefined
    })
}
exports.searchTracing = async (req, res) => {
    let { code } = req.body,
        sinister = await requestAxios('GET', `http://${req.get('host')}/api/sinisterClient/${code}`, '')

    res.render('summary', {
        sinister,
        urlBase: `http://${req.get('host')}`,
        user: (req.session.userLogged) ? req.session.userLogged : undefined
    })
}

exports.addClient = async (req, res) => {
    let { poliza, company, nroSinister } = req.body
    let theInsurance = await requestAxios('GET', `http://${req.get('host')}/api/insurance/${poliza}/${company}`, '')
    if (theInsurance.message === 'Poliza no encontrada.') { // not found the poliza
        // save client
        let client = {
            cuil: Number(req.body.cuil),
            lastname: req.body.lastname,
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone
        }
        let newClient = await requestAxios('POST', `http://${req.get('host')}/api/client`, { client })
        if (newClient.error) req.flash('error-client', newClient.message)
        theInsurance = await requestAxios('POST', `http://${req.get('host')}/api/insurance`, { company, poliza, oid: newClient._id })
        if (theInsurance.error) req.flash('error-insurance', theInsurance.message)

    } else {
        // update client
        theInsurance.client.phone = req.body.phone
        theInsurance.client.email = req.body.email
        let newClient = await requestAxios('PUT', `http://${req.get('host')}/api/client/contact`,
            { cuil: theInsurance.client.cuil, email: theInsurance.client.email, phone: theInsurance.client.phone })
        if (newClient.error) req.flash('error-client', newClient.message)
    }
    // with the client into the insurance, build new sinister 
    let newSinister = await requestAxios('POST', `http://${req.get('host')}/api/sinister`, { oid: theInsurance.client._id, nroSinister })
    if (newSinister.error) req.flash('error-sinister', newSinister.message)
    else req.flash('success', 'Se registró nuevo sinistro de forma exitosa.')
    console.log(newSinister)

    res.redirect('/company')
}

exports.viewloadPerito = async (req, res) => {
    let datos = req.body.codeSinister
    console.log(datos)
    // multimedia
    // if (Object.keys(req.files).length != 4) res.status(400).send({ status: false, message: `Faltan fotos del siniestro Nro ${nroSinister}` })
    // let paths = await multimedia.saveImage(nroSinister, req.files)

    let updateSinister = await requestAxios('PUT', `http://${req.get('host')}/api/updateSinester`,
        { sinister: datos })
    console.log(updateSinister)
    res.redirect('/proficient')
}
