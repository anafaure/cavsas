const axios = require('axios'),
    { switchPathRole } = require('./../utils'),
    Sinister = require('../models/sinister'),
    SinisterState = require('../models/sinisterState')
///////////////////////////////////////////////////////////////////////////////////////////////////////
exports.viewHome = (req, res) => { res.redirect(switchPathRole(req.session.userLogged.role)) }
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////s
exports.viewUsers = async (req, res) => {
    let users = await axios.get(`http://${req.get('host')}/api/user`).then(data => { return data.data })
    res.render('admin/user', {
        urlBase: `http://${req.get('host')}`,
        user: req.session.userLogged,
        users
    })
}
exports.viewCompany = async (req, res) => {
    let companies = await axios.get(`http://${req.get('host')}/api/company`).then(data => { return data.data })
    res.render('admin/company', {
        urlBase: `http://${req.get('host')}`,
        user: req.session.userLogged,
        companies
    })
}
exports.viewDashboard = async (req, res) => {
    if (req.session.userLogged.role === 'ADMIN') {
        let sinisters = await axios.get(`http://${req.get('host')}/api/sinister`).then(data => { return data.data })
        res.render('admin/admin', {
            sinisters,
            urlBase: `http://${req.get('host')}`,
            user: req.session.userLogged
        })
    }
    else res.redirect('/home')
}


///////////////////////////////////////////////////////////////////////////////////////////////////////
exports.viewCompania = (req, res) => {
    if (req.session.userLogged.role === 'SEGURO') {
        let errClient = req.flash('error-client'),
            errInsurance = req.flash('error-insurance'),
            errSinister = req.flash('error-sinister'),
            success = req.flash('success')

        res.render('aseguradora', {
            errClient: (errClient) ? errClient : undefined,
            errInsurance: (errInsurance) ? errInsurance : undefined,
            errSinister: (errSinister) ? errSinister : undefined,
            success: (success) ? success : undefined,
            urlBase: `http://${req.get('host')}`,
            user: req.session.userLogged
        })
    }
    else res.redirect('/home')
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
exports.viewTallerista = async(req, res) => {
    if (req.session.userLogged.role === 'TALLERISTA') {
        //asigna a la variable los siniestros que existan para ese tallerista
        let sinistersAssinged = await axios.get(`http://${req.get('host')}/api/sinisterBy/${req.session.userLogged.cuil}`).then(data => { return data.data })
        res.render('tallerista', {
            sinisters: sinistersAssinged,
            urlBase: `http://${req.get('host')}`,
            user: req.session.userLogged
        })
          
    }
    else res.redirect('/home')
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
exports.viewPerito = async (req, res) => {
    if (req.session.userLogged.role === 'PERITO') {
        let sinistersAssinged = await axios.get(`http://${req.get('host')}/api/sinisterBy/${req.session.userLogged.cuil}`).then(data => { return data.data })
        res.render('perito/perito', {
            sinisters: sinistersAssinged,
            urlBase: `http://${req.get('host')}`,
            user: req.session.userLogged
        })
    }
     else res.redirect('/home')
}
exports.viewPeritoEdit = async (req, res) => {
    let { oid } = req.params,
        sinister = await axios.get(`http://${req.get('host')}/api/sinister/${oid}`).then(data => { return data.data }).catch(err => { console.log(err) })
     
    res.render('perito/edit', {
        sinister,
        urlBase: `http://${req.get('host')}`,
        user: req.session.userLogged
    })
}
exports.loadSiniter = async (req, res) => {
    console.log(req.body)
    res.redirect('/proficient')
}