const Sinister = require('../../models/sinister'),
    User = require('../../models/user'),
    Company = require('../../models/company'),
    Insurance = require('../../models/insurance'),
    Licence = require('../../models/licence'),
    SinisterState = require('../../models/sinisterState')

exports.generateCode = async (req, res) => {
    let { data } = req.body
    let company = await Company.findOne({ id: data.company })
    let insured = await Insurance.findOne({
        company: company._id,
        poliza: Number(data.poliza)
    }).populate('client')
    if (insured) {
        if (insured.client.email === data.email && insured.client.cuil === Number(data.cuil)) {
            let sinisters = await Sinister.find({ client: insured.client._id })
                .populate('client')
                .sort({ createdAt: 'desc' })
            res.send({ sinisters, code: sinisters[0]._id, state: true })
        } else {
            res.send({ state: false, message: 'Los datos ingresados no son correctos. Por favor, revíselos.' })
        }
    } else {
        res.send({ state: false, message: 'Usted no es un asegurado.' })
    }
}
exports.addInsurance = async (req, res) => {
    let { company, poliza, oid } = req.body
    let newInsurance = new Insurance({
        company,
        poliza: Number(poliza),
        client: oid
    })
    newInsurance.save((err, i) => {
        if (err) res.status(500).send({ message: "Error al crear el seguro", error: err })
        else {
            Insurance.findOne({ _id: i._id }).populate('client').then(insurance => {
                res.send(insurance)
            })
        }
    })
}
exports.getInsurance = async (req, res) => {
    let { poliza, company } = req.params
    let insurance = await Insurance.findOne({ company, poliza }).populate('client')
    console.log(insurance)
    if (insurance === null || insurance === undefined) res.json({ message: "Poliza no encontrada." })
    else res.json(insurance)

}

exports.addSinister = async (req, res) => {
    let { oid, nroSinister } = req.body
    let newSinister = new Sinister({ client: oid, nroSinister }) 

    newSinister.save((err, s) => {
        if (err) res.status(500).send({ message: "Error al crear el siniestro", error: err })
        else res.send(s)
    })
}

exports.addSinisterState = async (req, res) => {
    let newSinisterState = new SinisterState({ descripcion: req.body.descripcion })
    newSinisterState.save((err, sinisterState) => {
        if (err) res.status(500).send({ message: "Error en la carga", error: err })
        else res.send(sinisterState)
    })
}

exports.assignProfessionals = async (req, res) => {
    console.log(req.body)
    let { idSinister, cuilPerito, cuilTallerista } = req.body
    let thePerito = null, theTallerista = null
    if (cuilPerito !== null) thePerito = await User.findOne({ cuil: cuilPerito })
    if (cuilTallerista !== null) theTallerista = await User.findOne({ cuil: cuilTallerista })
    let state = await SinisterState.findOne({ descripcion: "Iniciado" })

    // console.log(cuilPerito, thePerito)
    // console.log(cuilTallerista, theTallerista)
    Sinister.findOneAndUpdate({ _id: idSinister }, { $set: { perito: thePerito, tallerista: theTallerista, sinesterState: state } }, (err, sin) => {
        if (err) { console.log(err); res.status(500).send({ message: "Error en la actualización de datos" }) }
        if (!sin) {
            res.status(404).send({ message: "No se encontró el siniestro" })
        } else {
            res.send(sin)
        }
    })
}
exports.getSinister = async (req, res) => {
    let { code } = req.params
    //modifico el estado del siniestro cuando el perito ingresa a completar en formulario
    let state = await SinisterState.findOne({ descripcion: "En proceso de peritaje" })
    Sinister.update({ _id: code }, { $set: { sinesterState: state } })
    //fin de uptada
    Sinister.findOne({ _id: code }).populate('client')
        .populate('perito', 'lastname name cuil')
        .populate('tallerista', 'lastname name cuil')
        .exec((err, sinister) => {
            if (err) res.send({ message: "Siniestro no encontrado no encontrado.", code })
            else res.send(sinister)
        })

    // if (sinister) res.send(sinister)
    // else res.send({ message: "Siniestro no encontrado no encontrado.", code })
}
exports.getSinisterClient = async (req, res) => {
    let { code } = req.params

    Sinister.findOne({ _id: code }).populate('client')
        .populate('perito', 'lastname name cuil')
        .populate('tallerista', 'lastname name cuil')
        .exec((err, sinister) => {
            if (err) res.send({ message: "Siniestro no encontrado no encontrado.", code })
            else res.send(sinister)
            console.log(sinister)
        })
    // if (sinister) res.send(sinister)
    // else res.send({ message: "Siniestro no encontrado no encontrado.", code })
}
exports.getAllSinister = async (req, res) => {
    Sinister.find({}).populate('client').populate('perito').populate('tallerista').exec((err, sinisters) => {
        if (err) res.send([])
        else res.send(sinisters)
    })
}
exports.getSinisterAssing = async (req, res) => {
    let { cuil } = req.params, obj = {},
        user = await User.findOne({ cuil: cuil })
    if (user.role === 'PERITO') { obj = { perito: user._id } }
    if (user.role === 'TALLERISTA ') { obj = { tallerista: user._id } }
    let theSinisters = await Sinister.find(obj)
        .populate('client tallerista perito client.licence')//esta sentencia es de MONGO permite especificar que entidad traer como objeto
        .sort('asc')

    res.send(theSinisters)
}
exports.updatePerito = async (req, res) =>{
    console.log(req.params)
    let { code } = req.params
    //modifico el estado del siniestro cuando el perito ingresa a completar en formulario
    let state = await Sinister.findOne({ descripcion: "En proceso de peritaje" })
    Sinister.update({ _id: code }, { $set: { sinesterState: state } })
   res.json({ Sinister });
}

