const User = require('../../models/user'),
    Company = require('../../models/company'),
    { mayusculas } = require('../../utils')

exports.getAllUsers = async (req, res) => {
    let users = await User.find({})
    res.send(users)
}
exports.getUser = async (req, res) => {
    let { cuil } = req.params
    let user = await User.findOne({ cuil: cuil })
    res.send(user)
}
exports.getUsersByRole = async (req, res) => {
    let { role } = req.params,
        users = await User.find({ role: mayusculas(role) }),
        aux = users.map(u => { return { id: u.cuil, descripcion: u.lastname + ', ' + u.name } })

    res.send(aux)
}
exports.addUser = async (req, res) => {
    let company = (Number(req.body.company) !== 0) ? await Company.findOne({ id: req.body.company }) : null
    let user = new User({
        username: req.body.username,
        name: req.body.name,
        role: req.body.role,
        lastname: req.body.lastname,
        company: company,
        email: req.body.email,
        phone: req.body.phone,
        cuil: req.body.cuil
    })
    // console.log(user)
    // res.send({user})
    user.setPassword(req.body.password)
    user.save((err, user) => {
        if (err) res.status(500).send({ message: "Error en la registración de usuario", error: err })
        else res.send(user)
    })
}

exports.addCompany = (req, res) => {
    let { id, descripcion, cuit, razonSocial } = req.body
    razonSocial = (razonSocial) ? razonSocial : ''
    Company.findOneAndUpdate({ id }, { id, descripcion, cuit, razonSocial }, { upsert: true, new: true }, (err, compnay) => {
        if (err) { console.log(err); res.status(500).json({ error: err }) }
        else res.json(company)
    })
}
exports.getCompany = async (req, res) => {
    let companies = await Company.find({})
    res.json(companies)
}