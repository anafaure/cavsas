const Province = require('../../models/ubication/province'),
    Departament = require('../../models/ubication/departament'),
    City = require('../../models/ubication/city'),
    axios = require('axios')

exports.addProvince = async (req, res) => {
    let newProv = new Province({ id: req.body.id, descripcion: req.body.descripcion })
    newProv.save((err, prov) => {
        if (err) res.status(500).send({ message: "Error en la carga de la provincia", error: err })
        else res.send(prov)
    })
}
exports.addDepartament = async (req, res) => {
    let prov = await Province.findOne({ id: req.body.province.id }),
        newDpto = new Departament({ id: req.body.id, descripcion: req.body.descripcion, province: prov._id })
        console.log(prov._id);
        
    newDpto.save((err, dpto) => {
        if (err) res.status(500).send({ message: "Error en la carga de departemento", error: err })
        else res.send(dpto)
    })
}
exports.getAllProvince = async (req, res) => {
    let provinces = await Province.find({})
    res.send(provinces)
}
exports.getAllCity = async (req, res) => {
    let cities = await City.find({}).populate('departament')
    res.send(cities)
}
exports.getAllDepto = async (req, res) => {
    let dptos = await Departament.find({}).populate('province')
    res.send(dptos)
}
exports.getDptoByProvince = async (req, res) => {
    let { id } = req.params
    let dptos = await Departament.find({}).populate('province')
    let filtraded = dptos.filter(x => { if (Number(x.province.id) === Number(id)) { return x } })
    res.send(filtraded)
}
exports.getCityByDpto = async (req, res) => {
    let { id } = req.params
    let cities = await City.find({}).populate('departament')
    let filtraded = cities.filter(x => { if (Number(x.departament.id) === Number(id)) { return x } })
    res.send(filtraded)
}
exports.getCity = async (req, res) => {
    let { id } = req.params,
        theCity = await City.findOne({ id: Number(id) })
    if (theCity) res.send(theCity)
    else res.send({ descripcion: "Ciudad no encontrada.", id: id })
}
exports.addCity = async (req, res) => {
    let dpto = await Departament.findOne({ id: req.body.departament.id }),
        newCity = new City({ id: req.body.id, descripcion: req.body.descripcion, departament: dpto._id })
    newCity.save((err, city) => {
        if (err) res.status(500).send({ message: "Error en la carga de la localidad", error: err, city: newCity })
        else res.send(city)
    })
}
exports.etl = async (req, res) => {
    let dptos = await Departament.find({})
    let cities = await axios.get('http://localhost:3000/static/resource/localidades.json').then(data => {
        return data.data
    })
    // console.log(provinces.length);
    let promises = [], aux
    dptos.forEach(d => {
        cities.forEach(c => {
            if (Number(d.id) === Number(c.departamento.id)) {
                aux =  new City({ id: c.id, descripcion: c.descripcion, departament: d._id })
                promises.push(aux.save())
            }
        })
    })

    // Promise.all(promises).then(data=>{
    //     console.log(data.length);
    //     res.json(data)
    // })
    res.json(promises)
}