const Client = require('../../models/client'),
    Vehicule = require('../../models/vehicule'),
    Licence = require('../../models/licence')

exports.getClient = async (req, res) => {
    let { cuil } = req.params
    let client = await Client.findOne({ cuil: cuil })
    if (client) res.send(client)
    else res.send({ message: "Cliente no encontrado.", cuil })
    console.log(client);
}
exports.getAllClient = async (req, res) => {
    let clients = await Client.find({})
    res.send(clients)
}
exports.updateClientContact = async (req, res) => {
    let { cuil, email, phone } = req.body
    Client.findOneAndUpdate({ cuil: cuil }, { $set: { email: email, phone: phone } }, (err, clie) => {
        if (err) { console.log(err); res.status(500).send({ message: "Error en la actualización de datos" }) }
        if (!clie) {
            res.status(404).send({ message: "No existe el cliente" })
        } else {
            res.send(clie)
        }
    })
}
exports.addClient = async (req, res) => {
    let { client } = req.body
    let newClient = new Client({
        cuil: client.cuil,
        lastname: client.lastname,
        name: client.name,
        phone: client.phone,
        email: client.email
    })
    newClient.save((err, c) => {
        if (err) { res.status(500).send({ message: "Error al crear el cliente", error: err }) }
        else { res.send(c) }
    })
}

// exports.getLicence = async (req, res) => {
//     let { cuil } = req.params,
//     client = await 
// }
// exports.getVehicule = (req, res) => {
//     let { cuil } = req.params
// }
exports.addLicence = async (req, res) => {
    let { cuil, clase, nro, fechaExpedicion, fechaVencimiento, otorgado } = req.body,
        c = await Client.findOne({ cuil })
    newLicence = await Licence({ clase, nro, fechaExpedicion, fechaVencimiento, otorgado })
    await newLicence.save()
    c.licence.push(newLicence._id)
    let client = await Client.findOneAndUpdate({ cuil }, { $set: { licence: c.licence } }, { new: true })
    res.send(client)
}
exports.addVehicule = async (req, res) => {
    let { cuil, marca, modelo, dominio, año, color, nroChasis, nroMotor, tipo, inspeccionTecnica, tipoCargaTransportada } = req.body,
        c = await Client.findOne({ cuil })
    newVehicule = await Vehicule({ marca, modelo, dominio, año, color, nroChasis, nroMotor, tipo, inspeccionTecnica, tipoCargaTransportada })
    await newVehicule.save()
    c.vehicule.push(newVehicule._id)
    let client = await Client.findOneAndUpdate({ cuil }, { $set: { vehicule: c.vehicule } }, { new: true })
    res.send(client)
}

exports.updateClient = (req, res) => {
    console.log(req.body)
    res.send('updated')
}