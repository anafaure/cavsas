const User = require('./../models/user'),
    { switchPathRole } = require('./../utils')

exports.validate = (req, res) => {
    let { username, password } = req.body
    username = (!username) ? '' : username
    password = (!password) ? '' : password
    if (req.session.userLogged) res.redirect(switchPathRole(user.role))
    else {
        User.findOne({ username: username }, (err, user) => {
            if (err) res.send('Ha habido un error al registrar')
            req.flash('username', username)
            if (user === null) {
                req.flash('not-user', 'Usuario no encontrado')
                res.redirect('/login')
            }
            else {
                if (user.validPassword(password)) {
                    req.session.userLogged = {
                        id: user._id,
                        username: user.username,
                        email: user.email,
                        cuil: user.cuil,
                        name: user.name,
                        lastname: user.lastname,
                        phone: user.phone,
                        role: user.role, 
                        company: (user.company!==null)?user.company:''
                    }
                    res.redirect(switchPathRole(user.role))
                } else {
                    req.flash('bad-password', 'Contraseña no válida.')
                    res.redirect('/login');
                }
            }
        })
    }

}
exports.logout = (req, res) => {
    req.session.destroy((err) => {
        if (err) { res.negotiate(err) }
        res.redirect('/')
    })
}
exports.viewLogin = (req, res) => {
    if (!req.session.userLogged) {
        let username = req.flash('username'),
            msgUser = req.flash('not-user'),
            msgPass = req.flash('bad-password')
        res.render('login', {
            urlBase: `http://${req.get('host')}`,
            msgUser: (msgUser.length === 0) ? undefined : msgUser,
            msgPassword: (msgPass.length === 0) ? undefined : msgPass,
            username
        })
    } else res.redirect('/')
}