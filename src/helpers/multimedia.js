const fse = require('fs-extra'),
    path = require('path')

const saveImge = async (sid, file, part) => {
    let auxPath = `resource/sinisters/${sid}/images/`,
        pathFull = path.join(__dirname, `../../public/${auxPath}`),
        name = `${part}-${new Date().getTime()}.${file.mimetype.split('/')[1]}`
    await fse.ensureDir(pathFull)
        .then(() => file.mv(`${pathFull}${name}`))
        .catch(err => console.log(err))
    return { pathFile: `/static/${auxPath}${name}`, typeFile: file.mimetype }
}



module.exports.saveImage = async (sid, files) => {
    let paths = []
    if (files.imgFront) paths.push(await saveImge(sid, files.imgFront, 'front'))
    if (files.imgBack) paths.push(await saveImge(sid, files.imgBack, 'back'))
    if (files.imgLeft) paths.push(await saveImge(sid, files.imgLeft, 'left'))
    if (files.imgRight) paths.push(await saveImge(sid, files.imgRight, 'right'))
    return paths
}