let config = {
    port: 3000,
    session: {
        secretPass: 'C4vs4s',
        maxAge: 36000000
    },
    pathDatabase: 'mongodb://localhost:27017/cavsas',
    encrypt: 'C4vs4sEncryp'
}

module.exports = config