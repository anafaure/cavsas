let config = {
    port: 0000,
    session:{
        secretPass: 'secret',
        maxAge: 123
    },
    pathDatabase: 'mongodb://user:pass@server:port/database',
    email: {
        host: 'host',
        port: 'port',
        secure: 'secure',
        requireTLS: 'require',
        username: 'email',
        password: 'pass'
    },
}

module.exports = config