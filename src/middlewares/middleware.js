exports.isAuthenticated = (req, res, next) => {
    let u = req.session.userLogged
    if (u) next()
    else res.redirect('/login') 
}